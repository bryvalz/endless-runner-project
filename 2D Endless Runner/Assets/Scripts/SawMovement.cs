﻿using UnityEngine;

public class SawMovement : MonoBehaviour
{
    private Transform sawPos;
    private Vector2 batasKiri, batasKanan;
    private bool moveRight;

    public float speed;
    public float left;
    public float right;

    private void Awake()
    {
        sawPos = GetComponent<Transform>();
    }

    void Start()
    {
        moveRight = true;

        batasKiri = new Vector2(sawPos.localPosition.x - left, sawPos.position.y);
        batasKanan = new Vector2(sawPos.localPosition.x + right, sawPos.position.y);
    }

    private void Update()
    {
        if (moveRight)
        {
            transform.Translate(Vector2.right * speed * Time.deltaTime);
        }

        else
        {
            transform.Translate(Vector2.left * speed * Time.deltaTime);
        }

        if (sawPos.localPosition.x <= batasKiri.x)
        {
            moveRight = true;
        }

        else if (sawPos.localPosition.x >= batasKanan.x)
        {
            moveRight = false;
        }
    }
}
